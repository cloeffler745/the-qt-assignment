#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <vector>
#include <algorithm>

Ui::MainWindow hi;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{

    delete ui;
}

double MainWindow::HWScore(){
    std::vector<int> Homework;
    if(ui->ClassCB->currentText() == "PIC 10 B Intermediate Programming ")
        Homework = {ui->HW1B->value(), ui->HW2B->value(), ui->HW3B->value(), ui->HW4B->value(), ui->HW5B->value(), ui->HW6B->value(), ui->HW7B->value(), ui->HW8B->value()};
    else{
        Homework = {ui->HW1B->value(), ui->HW2B->value(), ui->HW3B->value()};
    }
    //find the smallest HW score and remove it
    std::vector<int>::iterator x = Homework.begin();
    std::vector<int>::iterator i = Homework.begin();
    while(i != Homework.end()){
        int y = *i;
        if(y < *x)
            x = i;
        i++;
    }
    Homework.erase(x);

    //Homework average

    double avg = 0;
    std::vector<int>::iterator start = Homework.begin();
    std::vector<int>::iterator stop = Homework.end();
    std::for_each(start, stop, [=,&avg](double m){avg += m;});
    if(ui->ClassCB->currentText() == "PIC 10 B Intermediate Programming ")
        avg /= 700.0;
    else
        avg /= 200.0;

    double Score = avg*0.25;
    return Score;
}

void MainWindow::ChangeLabel(){
    double s = 0;
    s += HWScore();
    if(ui->ClassCB->currentText() == "PIC 10 B Intermediate Programming "){
        if(ui->MID1B->value() >= ui->MID2B->value())
            s += (ui->MID1B->value()/100.0) * 0.30;
        else
            s += (ui->MID2B->value()/100.0) * 0.30;

    }
    else{
        s += (ui->MID1B->value()/100.0) * 0.30;
    }
    s += (ui->FINAB->value()/100.0) * 0.45;
    s *= 100.0;
    QString t = QString::number(s);
    ui->ScoreL->setText(t);
}

void MainWindow::ChangeLabelA(){
    double s = HWScore();
    s += (ui->MID1B->value()/100.0) * 0.20;
    s += (ui->MID2B->value()/100.0) * 0.20;
    s += (ui->FINAB->value()/100.0) * 0.35;
    s *= 100.0;
    QString t = QString::number(s);
    ui->ScoreL->setText(t);
}

void MainWindow::PIC10B(QString I){
    if(I == "PIC 10 C Advanced Programming"){
        //hide number boxes 4-8, MID2
        ui->HW4B->hide();
        ui->HW5B->hide();
        ui->HW6B->hide();
        ui->HW7B->hide();
        ui->HW8B->hide();
        ui->MID2B->hide();
        //hide scroll bars 4-8, MID2
        ui->HW4S->hide();
        ui->HW5S->hide();
        ui->HW6S->hide();
        ui->HW7S->hide();
        ui->HW8S->hide();
        ui->MID2S->hide();
        //hide labels
        ui->label_6->hide();
        ui->label_3->hide();
        ui->label_8->hide();
        ui->label_5->hide();
        ui->label_2->hide();
        ui->label_10->hide();
        //button changes
        ui->SchemaAPB->hide();
        ui->SchemaBPB->setText("&Calculate Score");
        ui->SchemaBPB->setToolTip("Calculate score based on above scores");
        //Score reset
        ui->ScoreL->setText("");
        ui->HW1S->setValue(0);
        ui->HW2S->setValue(0);
        ui->HW3S->setValue(0);
        ui->HW4S->setValue(0);
        ui->HW5S->setValue(0);
        ui->HW6S->setValue(0);
        ui->HW7S->setValue(0);
        ui->HW8S->setValue(0);
        ui->MID1S->setValue(0);
        ui->MID2S->setValue(0);
        ui->FINALS->setValue(0);
    }
    else{
        //show number boxes 4-8, MID2
        ui->HW4B->show();
        ui->HW5B->show();
        ui->HW6B->show();
        ui->HW7B->show();
        ui->HW8B->show();
        ui->MID2B->show();
        //show scroll bars 4-8, MID2
        ui->HW4S->show();
        ui->HW5S->show();
        ui->HW6S->show();
        ui->HW7S->show();
        ui->HW8S->show();
        ui->MID2S->show();
        //show labels
        ui->label_6->show();
        ui->label_3->show();
        ui->label_8->show();
        ui->label_5->show();
        ui->label_2->show();
        ui->label_10->show();
        //button changes
        ui->SchemaAPB->show();
        ui->SchemaBPB->setText("Schema &B");
        ui->SchemaBPB->setToolTip("Drop lowest midterm");
        //score reset
        ui->ScoreL->setText("");
        ui->HW1S->setValue(0);
        ui->HW2S->setValue(0);
        ui->HW3S->setValue(0);
        ui->HW4S->setValue(0);
        ui->HW5S->setValue(0);
        ui->HW6S->setValue(0);
        ui->HW7S->setValue(0);
        ui->HW8S->setValue(0);
        ui->MID1S->setValue(0);
        ui->MID2S->setValue(0);
        ui->FINALS->setValue(0);
    }
}
